import 'package:flutter/material.dart';

const ColorPrimary = Color(0xff262A4D);
const ColorBackground = Color(0xffF1F5F9);
const ColorBackgroundDark = Color(0xff1D1F37);
const ColorWhite = Color(0xffFFFFFF);
const ColorAccent = Color(0xff3B5FDE);
