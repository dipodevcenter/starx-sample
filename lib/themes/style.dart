import 'package:flutter/material.dart';
import 'package:starx/themes/color.dart';

class StarXAppTheme {
  Color backgroundDark;
  Color primaryDark;
  bool isDark;

  /// Default constructor
  StarXAppTheme({@required this.isDark});

  ThemeData get themeData {
    String fontFamily = 'Quicksand';
    /// Create a TextTheme and ColorScheme, that we can use to generate ThemeData
    TextTheme txtTheme =
        (isDark ? ThemeData.dark() : ThemeData.light()).textTheme.apply(fontFamily: fontFamily);
    Color txtColor = txtTheme.bodyText1.color;
    ColorScheme colorScheme = ColorScheme(
        // Decide how you want to apply your own custom them, to the MaterialApp
        brightness: isDark ? Brightness.dark : Brightness.light,
        primary: primaryDark ?? primaryDark ?? ColorPrimary,
        primaryVariant: primaryDark ?? primaryDark ?? ColorPrimary,
        background: backgroundDark ?? backgroundDark ?? ColorBackground,
        secondary: primaryDark ?? primaryDark ?? ColorPrimary,
        secondaryVariant: primaryDark ?? primaryDark ?? ColorPrimary,
        surface: primaryDark ?? primaryDark ?? ColorPrimary,
        onBackground: txtColor,
        onSurface: txtColor,
        onError: Colors.white,
        onPrimary: Colors.white,
        onSecondary: Colors.white,
        error: Colors.red.shade400);

    /// Now that we have ColorScheme and TextTheme, we can create the ThemeData
    var t = ThemeData.from(textTheme: txtTheme, colorScheme: colorScheme)
        // We can also add on some extra properties that ColorScheme seems to miss
        .copyWith(
          buttonColor: primaryDark ?? primaryDark ?? ColorPrimary,
          cursorColor: primaryDark ?? primaryDark ?? ColorPrimary,
          highlightColor: primaryDark ?? primaryDark ?? ColorPrimary,
          toggleableActiveColor: primaryDark ?? primaryDark ?? ColorPrimary,
        );

    /// Return the themeData which MaterialApp can now use
    return t;
  }
}
