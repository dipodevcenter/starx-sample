import 'package:flutter/material.dart';

// Import Widget
import 'package:starx/screens/homepage/widgets/features/Features.dart';
import 'package:starx/screens/homepage/widgets/header/Header.dart';
import 'package:starx/screens/homepage/widgets/promo/Promo.dart';

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Header(),
        Features(),
        Promo(),
      ],
    );
  }
}
