import 'package:flutter/material.dart';

class Features extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(
                  'Catagories',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                GestureDetector(
                  onTap: () {},
                  child: Text('See More',
                      style: TextStyle(color: Color(0xff3B5FDE))),
                ),
              ],
            ),
          ),
          GridView.count(
            crossAxisCount: 4,
            shrinkWrap: true,
            physics: BouncingScrollPhysics(),
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Card(
                      color: Colors.white,
                      margin: EdgeInsets.only(bottom: 8),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      child: Padding(
                        padding: EdgeInsets.all(8),
                        child: Container(
                          height: 36,
                          child: Image.asset('assets/images/feat_vehicle.png',
                              fit: BoxFit.fill),
                        ),
                      )),
                  Text('My Vehicle')
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Card(
                      color: Colors.white,
                      margin: EdgeInsets.only(bottom: 8),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      child: Padding(
                        padding: EdgeInsets.all(8),
                        child: Container(
                          height: 36,
                          child: Image.asset(
                              'assets/images/feat_aggrements.png',
                              fit: BoxFit.fill),
                        ),
                      )),
                  Text('Aggrement')
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Card(
                      color: Colors.white,
                      margin: EdgeInsets.only(bottom: 8),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      child: Padding(
                        padding: EdgeInsets.all(8),
                        child: Container(
                          height: 36,
                          child: Image.asset(
                              'assets/images/feat_service_book.png',
                              fit: BoxFit.fill),
                        ),
                      )),
                  Text('Service Book')
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Card(
                      color: Colors.white,
                      margin: EdgeInsets.only(bottom: 8),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      child: Padding(
                        padding: EdgeInsets.all(8),
                        child: Container(
                          height: 36,
                          child: Image.asset('assets/images/feat_shop.png',
                              fit: BoxFit.fill),
                        ),
                      )),
                  Text('Find Shop')
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Card(
                      color: Colors.white,
                      margin: EdgeInsets.only(bottom: 8),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      child: Padding(
                        padding: EdgeInsets.all(8),
                        child: Container(
                          height: 36,
                          child: Image.asset('assets/images/feat_smart_key.png',
                              fit: BoxFit.fill),
                        ),
                      )),
                  Text('Smart Key')
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Card(
                      color: Colors.white,
                      margin: EdgeInsets.only(bottom: 8),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      child: Padding(
                        padding: EdgeInsets.all(8),
                        child: Container(
                          height: 36,
                          child: Image.asset('assets/images/feat_finance.png',
                              fit: BoxFit.fill),
                        ),
                      )),
                  Text('Finance')
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Card(
                      color: Colors.white,
                      margin: EdgeInsets.only(bottom: 8),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      child: Padding(
                        padding: EdgeInsets.all(8),
                        child: Container(
                          height: 36,
                          child: Image.asset('assets/images/feat_insurance.png',
                              fit: BoxFit.fill),
                        ),
                      )),
                  Text('Isurance')
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Card(
                      color: Colors.white,
                      margin: EdgeInsets.only(bottom: 8),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      child: Padding(
                        padding: EdgeInsets.all(8),
                        child: Container(
                          height: 36,
                          child: Image.asset('assets/images/feat_send_car.png',
                              fit: BoxFit.fill),
                        ),
                      )),
                  Text('Send Car')
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
