import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Import Utils
import 'package:starx/themes/color.dart';
import 'package:starx/widgets/button/button.dart';

// Import Widgets
import 'package:starx/widgets/card/card.dart';

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          height: 80,
          color: ColorPrimary,
        ),
        Container(
          alignment: Alignment.topCenter,
          child: AppCard(
            margin: EdgeInsets.fromLTRB(24, 36, 24, 0),
            child: Padding(
              padding: EdgeInsets.all(16),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Wrap(
                    children: <Widget>[
                      Text('Hai,', style: TextStyle(fontSize: 20)),
                      Text('Guest',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold)),
                    ],
                  ),
                  Wrap(
                    children: <Widget>[
                      Button(
                        value: 'LOGIN',
                        onPress: () {
                          showDialog(
                            context: (context),
                            builder: (_) => AlertDialog(
                              title: Text('Title Messages'),
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text('OK'),
                                  onPressed: () {
                                    Navigator.of(context).pop(false);
                                  },
                                )
                              ],
                            ),
                          );
                        },
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
