import 'package:flutter/material.dart';
import 'package:starx/screens/homepage/widgets/promo/promo_carousel.dart';

class Promo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(
                  'News & Promo',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).pushNamed('/promo-detail');
                  },
                  child: Text('See More',
                      style: TextStyle(color: Color(0xff3B5FDE))),
                ),
              ],
            ),
          ),
          PromoCarousel(),
        ],
      ),
    );
  }
}
