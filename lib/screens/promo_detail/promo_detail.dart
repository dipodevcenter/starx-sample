import 'package:flutter/material.dart';
import 'package:starx/themes/color.dart';
import 'package:starx/themes/style.dart';
import 'package:starx/widgets/button/button.dart';

// Import Global Variable
import 'package:starx/config/globals.dart';

class PromoDetail extends StatefulWidget {
  @override
  _PromoDetailState createState() => _PromoDetailState();
}

class _PromoDetailState extends State<PromoDetail> {
  @override
  Widget build(BuildContext context) {
    StarXAppTheme appTheme = StarXAppTheme(isDark: isDark)
      ..primaryDark = isDark ? ColorPrimary : null
      ..backgroundDark = isDark ? ColorBackgroundDark : null;
    return MaterialApp(
      theme: appTheme.themeData,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: ColorPrimary,
          elevation: 0.0,
          centerTitle: false,
          titleSpacing: 24,
          title: Image.asset('assets/images/logo-light.png', height: 24),
        ),
        extendBodyBehindAppBar: true,
        body: Center(
          child: ListView(
            children: <Widget>[
              Text('This ini detail promo'),
              Button(
                value: 'Back to Home',
                onPress: () {
                  Navigator.of(context).pushNamed('/home');
                },
              ),
            ],
        ),
        ),
      ),
    );
  }
}