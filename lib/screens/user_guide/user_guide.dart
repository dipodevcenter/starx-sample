import 'package:flutter/material.dart';
import 'package:starx/themes/color.dart';

class UserGuide extends StatefulWidget {
  @override
  _UserGuideState createState() => _UserGuideState();
}

class _UserGuideState extends State<UserGuide> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: ColorBackground),
      child: Center(
        child: Text('this is User Guide Page'),
      ),
    );
  }
}