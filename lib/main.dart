import 'package:flutter/material.dart';

// Import Global Variable
import 'package:starx/config/globals.dart';

// Import Screen
import 'package:starx/screens/homepage/views/Homepage.dart';
import 'package:starx/screens/promo_detail/promo_detail.dart';
import 'package:starx/screens/user_guide/user_guide.dart';

// Import Utils
import 'package:starx/themes/color.dart';
import 'package:starx/themes/style.dart';

// Import Common
import 'package:starx/common/ActionNavigation/ActionNotification.dart';
import 'package:starx/common/BottomBar/BottomBar.dart';

class Main extends StatefulWidget {
  @override
  _MainState createState() => _MainState();
}

class _MainState extends State<Main> {
  int isActiveTab = activeTab;

  var tabIndex = {
    0: Homepage(),
    1: UserGuide(),
  };

  @override
  Widget build(BuildContext context) {
    StarXAppTheme appTheme = StarXAppTheme(isDark: isDark)
      ..primaryDark = isDark ? ColorPrimary : null
      ..backgroundDark = isDark ? ColorBackgroundDark : null;
    return MaterialApp(
      theme: appTheme.themeData,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: ColorPrimary,
          elevation: 0.0,
          centerTitle: false,
          titleSpacing: 24,
          title: Image.asset('assets/images/logo-light.png', height: 24),
          actions: <Widget>[
            ActionNotification(),
          ],
        ),
        extendBodyBehindAppBar: true,
        body: tabIndex[isActiveTab],
        bottomNavigationBar: !isDetail && isVerify
            ? BottomBar(
                handleTab: (value) {
                  setState(() {
                    isActiveTab = value;
                  });
                },
              )
            : null,
      ),
      routes: {
        '/promo-detail': (_) => PromoDetail(),
        '/home': (_) => Main()
      },
    );
  }
}

void main() => runApp(Main());
