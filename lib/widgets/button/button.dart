import 'package:flutter/material.dart';

// Import Utils
import 'package:starx/themes/color.dart';

class Button extends StatelessWidget {
  final String value;
  final GestureTapCallback onPress;

  Button({
    Key key,
    @required this.value,
    @required this.onPress,
  }) : super(key : key) ;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton (
      fillColor: ColorAccent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
      child: Container(
        child: Text(
          value ?? value ?? 'BUTTON',
          style: TextStyle(color: Colors.white),
        ),
      ),
      onPressed: onPress,
    );
  }
}
