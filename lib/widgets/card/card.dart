import 'package:flutter/material.dart';

// Import Global Variable
import 'package:starx/config/globals.dart';
import 'package:starx/themes/color.dart';

class AppCard extends StatefulWidget {
  final Widget child;
  final EdgeInsetsGeometry margin;
  final Color backgroundColor;

  AppCard({
    Key key,
    @required this.child,
    this.margin,
    this.backgroundColor,
  }) : super(key: key);

  @override
  _AppCardState createState() => _AppCardState();
}

class _AppCardState extends State<AppCard> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Card(
          color: isDark ? ColorPrimary : ColorWhite,
          margin: widget.margin ?? EdgeInsets.all(24),
          child: widget.child,
        )
      ],
    );
  }
}
