import 'package:flutter/material.dart';

// Import Utils
import 'package:starx/themes/color.dart';

class ActionNotification extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16),
      child: Wrap(
        children: <Widget>[
          GestureDetector(
            child: Icon(
              Icons.notifications,
              color: ColorWhite,
              size: 24.0,
            ),
          ),
        ],
      ),
    );
  }
}
