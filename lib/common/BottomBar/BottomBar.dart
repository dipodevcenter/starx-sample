import 'package:flutter/material.dart';

// Import Utils
import 'package:starx/themes/color.dart';

class BottomBar extends StatefulWidget {
  final Function handleTab;

  BottomBar({
    Key key,
    this.handleTab
  }): super(key:key);

  @override
  _BottomBarState createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    widget.handleTab(index);
  }
  
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      elevation: 16,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text('Home'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.book),
          title: Text('User Guide'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.call),
          title: Text('Contact Us'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.menu),
          title: Text('Other'),
        ),
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: ColorAccent,
      onTap: _onItemTapped,
    );
  }
}
